from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response

from .serializers import BookingSerializer
from .models import Reservation, BookingInfo
from datetime import datetime


def create_items(array, obj):
    """
        A function to append dictionaries to a list
    """
    array.append(
        {
            'listing_type': obj.booking.listing.listing_type, 
            'title': obj.booking.listing.title, 
            'country': obj.booking.listing.country,
            'city': obj.booking.listing.city,
            'price': obj.booking.price
        }
    )
    return array


class ReservationAPI(APIView):
    """
        A view to return dates of listings which are not reserved 
        in respect to the dates coming from the Get request.
    """
    
    def get(self, request, *args, **kwargs):
        """
            A function that handles the get request for an endpoint example:
                http://localhost:8000/api/v1/units/?max_price=100&check_in=2021-12-09&check_out=2021-12-12
            
            @items - variable to store available listings
            @price - variable for user request listing max_price criteria
            @user_start_date - user request listing check_in criteria
            @user_end_date - user request listing check_out criteria
            @reservations - To filter all reservations
        """

        price = int(request.GET.get('max_price'))
        user_start_date = request.GET.get('check_in')
        user_start_date = datetime.strptime(user_start_date, '%Y-%m-%d').date()
        user_end_date = request.GET.get('check_out')
        user_end_date = datetime.strptime(user_end_date, '%Y-%m-%d').date()
        items = []
        reservations = Reservation.objects.filter().all()
        for n_dates in reservations:
            if n_dates.booking.price < price and n_dates.booking.listing != None:
                if ((user_start_date > n_dates.end) and (user_end_date > n_dates.end)) or (user_end_date < n_dates.start) :
                    create_items(items, n_dates)
        if items:
            return Response({'items': items})
        else:
            return Response({'error': 'Noreservations available'})
            