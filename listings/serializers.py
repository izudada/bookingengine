from rest_framework import serializers

from .models import BookingInfo

class BookingSerializer(serializers.ModelSerializer):

    class Meta:
        model = BookingInfo
        fields = ('listing.listing_type', 'listing.title', 'listing.country', 'listing.city', 'price')