from django.urls import path

from .views import ReservationAPI


urlpatterns = [
    path('', ReservationAPI.as_view(), name="reservation")
]