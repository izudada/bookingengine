from rest_framework.test import APITestCase
from django.urls import reverse

from listings.models import BookingInfo ,Reservation


class TestSetUP(APITestCase):

    def setUp(self):
        self.reservation_url = reverse('reservation')

        self.reservation_data = {
            'max_price': 100,
            'check_in': '2021-02-09',
            'check_out': '2021-12-12' 
        }

        self.booking1 = BookingInfo.objects.filter().first()
        self.reserved1 = Reservation.objects.create(
            booking=self.booking1,
            start='2021-02-09',
            end='2021-02-09'
        )

        return super().setUp()

    def tear_down(self):
        return super().tearDown