from django.test import SimpleTestCase
from django.urls import resolve

from listings.views import ReservationAPI
from .test_setup import TestSetUP


class TestUrls(TestSetUP, SimpleTestCase):

    def test_reservation_url_is_resolved(self):
         url = self.reservation_url
        #  self.assertEquals(resolve(url).func, ReservationAPI) # fails
         self.assertEquals(resolve(url).func.view_class, ReservationAPI)  # Passes