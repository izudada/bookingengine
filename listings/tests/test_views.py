from .test_setup import TestSetUP


class TestViews(TestSetUP):
    def test_reservation_endpoint_with_maxprice_100(self):
        res = self.client.get(self.reservation_url, self.reservation_data)
        print(res.data)
        self.assertEqual(res.status_code, 200)